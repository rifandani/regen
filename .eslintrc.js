module.exports = {
  extends: [
    'airbnb',
    'airbnb-typescript',
    'prettier',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
  ],
  plugins: ['@typescript-eslint'],
  env: {
    commonjs: false,
    node: true,
  },
  parserOptions: {
    project: ['./tsconfig.json'],
  },
  root: true,
  parser: '@typescript-eslint/parser',
  rules: {
    'no-console': 'off',
  },
};
