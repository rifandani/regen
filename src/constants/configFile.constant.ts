import type { Answers, QuestionCollection } from 'inquirer';
import {
  RegenComponentTypeNormal,
  RegenComponentTypeNormalObj,
  RegenProjectConfig
} from '../types/config.type';

// QuestionCollection<Answers> === Parameters<typeof prompt>[0]
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ConfigQuestions = QuestionCollection<Answers> & { [Symbol.iterator](): any };

// project level questions
export const projectLevelQuestions: ConfigQuestions = [
  {
    type: 'confirm',
    name: 'usesTypeScript',
    message: 'Does this project use TypeScript?',
    default: () => true,
  },
  {
    type: 'list',
    name: 'testLibrary',
    message: 'What testing library does your project use?',
    choices: ['Testing Library', 'Enzyme', 'None'],
    default: () => 'Testing Library',
  },
];

// component level questions
export const componentLevelQuestions: ConfigQuestions = [
  {
    type: 'input',
    name: 'component.default.path',
    message: 'Set the default path directory to where your components will be generated in?',
    default: () => 'src/components',
  },
  {
    type: 'confirm',
    name: 'component.default.usesTwinMacro',
    message: 'Does this project use Twin Macro?',
    default: () => true,
  },
  {
    type: 'list',
    name: 'component.default.withStyle',
    message: 'Choose your CSS Preprocessor?',
    choices: ['none', 'css', 'scss', 'less', 'styl'],
    default: () => 'none',
    validate: (
      input: RegenComponentTypeNormal['withStyle'],
      answers: (RegenProjectConfig & { component: RegenComponentTypeNormalObj }) | undefined
    ) =>
      // error if user input true `usesTwinMacro` and `withStyle` is not `'none'`
      answers?.component.default.usesTwinMacro && input !== 'none'
        ? `ValidationError: CSS Preprocessor should be set to 'none', if you already use Twin Macro`
        : true,
  },
  {
    type: 'confirm',
    name: 'component.default.usesCssModule',
    message: 'Would you like to create your corresponding stylesheet file as modules?',
    default: () => false,
    validate: (
      input: RegenComponentTypeNormal['usesCssModule'],
      answers: (RegenProjectConfig & { component: RegenComponentTypeNormalObj }) | undefined
    ) =>
      // error if user input true `usesTwinMacro` and `withStyle` is not `'none'`
      answers?.component.default.usesTwinMacro && input
        ? `ValidationError: CSS Modules should be set to 'No', if you already use Twin Macro`
        : true,
  },
  {
    type: 'confirm',
    name: 'component.default.withTest',
    message: 'Would you like to create a corresponding test file with each component you generate?',
    default: () => true,
  },
  {
    type: 'confirm',
    name: 'component.default.withStory',
    message: 'Would you like to create a corresponding story with each component you generate?',
    default: () => false,
  },
  {
    type: 'confirm',
    name: 'component.default.withLazy',
    message:
      'Would you like to create a corresponding lazy loaded component file with each component you generate?',
    default: () => false,
  },
];

// hook level questions
export const hookLevelQuestions: ConfigQuestions = [
  {
    type: 'input',
    name: 'hook.default.path',
    message: 'Set the default path directory to where your hooks will be generated in?',
    default: () => 'src/hooks',
  },
  {
    type: 'confirm',
    name: 'hook.default.withTest',
    message: 'Would you like to create a corresponding test file with each hook you generate?',
    default: () => true,
  },
];

// view level questions
export const viewLevelQuestions: ConfigQuestions = [
  {
    type: 'input',
    name: 'view.default.path',
    message: 'Set the default path directory to where your views will be generated in?',
    default: () => 'src/views',
  },
  {
    type: 'confirm',
    name: 'view.default.withRoute',
    message:
      'Would you like to create a corresponding route file with each view component you generate?',
    default: () => true,
  },
  {
    type: 'confirm',
    name: 'view.default.withViewModel',
    message:
      'Would you like to create a corresponding view model file with each view component you generate?',
    default: () => true,
  },
  {
    type: 'confirm',
    name: 'view.default.withTest',
    message:
      'Would you like to create a corresponding test file with each view component you generate?',
    default: () => true,
  },
];

// store level questions
export const storeLevelQuestions: ConfigQuestions = [
  {
    type: 'input',
    name: 'store.default.path',
    message: 'Set the default path directory to where your stores will be generated in?',
    default: () => 'src/stores',
  },
  {
    type: 'confirm',
    name: 'store.default.useEntityAdapter',
    message:
      'Would you rather normalize your state shape using entity adapter for each store you generate?',
    default: () => false,
  },
  {
    type: 'confirm',
    name: 'store.default.withTest',
    message: 'Would you like to create a corresponding test file with each store you generate?',
    default: () => true,
  },
];

// scaffold level questions
export const scaffoldLevelQuestions: ConfigQuestions = [
  {
    type: 'input',
    name: 'scaffold.default.url',
    message: 'Set the url to where we point your default scaffolds?',
    default: () => 'https://github.com/rifandani/frontend-template-rest.git',
  },
];

// merge all questions together
export const configQuestions = [
  ...projectLevelQuestions,
  ...componentLevelQuestions,
  ...hookLevelQuestions,
  ...viewLevelQuestions,
  ...storeLevelQuestions,
];
